﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoMVC.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

        public List<Title> Titles { get; set; } // Пользователь имеет много Списвков(Title)

        public int? RoleId { get; set; } // id Роли
        public Role Role { get; set; } // Пользователь имеет одну Роль

    }
}
