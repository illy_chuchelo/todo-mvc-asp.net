﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDoMVC.Models;

namespace ToDoMVC.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        ToDoContext db;
        public AdminController(ToDoContext context)
        {
            db = context;
        }

        [ActionName("Index")]
        public async Task<IActionResult> Index()
        {
            var lists = await db.Titles
                .Include(t => t.User)
                .Include(t => t.Tasks)
                .ToListAsync();
            return View(lists);
        }
    }
}
