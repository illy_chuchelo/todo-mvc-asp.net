﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ToDoMVC.Models;

namespace ToDoMVC.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        ToDoContext db;
        public HomeController(ToDoContext context)
        {
            db = context;
        }

        [ActionName("Index")]
        public async Task <IActionResult> Index()
        {
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            var lists = await db.Titles.Where(ti => ti.UserId == user.Id)
                .Include(t => t.Tasks)
                .ToListAsync();
            return View(lists);
        }
    }
}
