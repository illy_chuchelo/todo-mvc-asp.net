﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoMVC.Models;

namespace ToDoMVC.Controllers
{
    [Authorize]
    public class TitlesController : Controller
    {
        ToDoContext db;
        public TitlesController(ToDoContext context)
        {
            db = context;
        }

        // Создание нового Действия
        public IActionResult Create()
        {
            User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            ViewData["id"] = user.Id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Title title)
        {
            db.Titles.Add(title);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
        //------------

        // Редактирование 
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Title title = await db.Titles.FirstOrDefaultAsync(p => p.Id == id);
                if (title != null)
                    return View(title);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int? id, string Name)
        {
            if (id != null)
            {
                Title title = await db.Titles.FirstOrDefaultAsync(p => p.Id == id);
                if (title != null)
                {
                    title.Name = Name;
                    db.Titles.Update(title);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }
            }
            return NotFound();
        }
        // ----------------

        // Удаление
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id != null)
            {
                Title title = new Title { Id = id.Value };
                db.Entry(title).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");

            }
            return NotFound();
        }

        // ---------------
    }
}
