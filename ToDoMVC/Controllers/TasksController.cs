﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDoMVC.Models;
using Task = ToDoMVC.Models.Task;

namespace ToDoMVC.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {
        ToDoContext db;
        public TasksController(ToDoContext context)
        {
            db = context;
        } 
        
        // Создание нового Действия
        [HttpPost]        
        public async Task<IActionResult> Create(Task task)
        {
            db.Tasks.Add(task);
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
        //------------

        // Редактирование 
        public async Task<IActionResult> Edit(int? id)
        {
            if (id != null)
            {
                Task task = await db.Tasks.FirstOrDefaultAsync(p => p.Id == id);
                if (task != null)
                    return View(task);
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(int? id, string Name)
        {
            if (id != null)
            {
                Task task = await db.Tasks.FirstOrDefaultAsync(p => p.Id == id);
                if (task != null)
                {
                    task.Name = Name;
                    db.Tasks.Update(task);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index", "Home");
                }
            }
            return NotFound();
        }
        // ----------------

        // Удаление
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if(id != null)
            {               
                Task task = new Task { Id = id.Value };
                db.Entry(task).State = EntityState.Deleted;
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
                
            }
            return NotFound();
        }
        // ---------------

        public async Task<IActionResult> ToggleStatus(int? id)
        {
            if (id != null)
            {
                if (id != null)
                {
                    Task task = await db.Tasks.FirstOrDefaultAsync(p => p.Id == id);
                    if (task != null)
                    {
                        if(task.Done)
                        {
                            task.Done = false;
                            db.Tasks.Update(task);
                            //await db.SaveChangesAsync();
                        }
                        else
                        {
                            task.Done = true;
                            db.Tasks.Update(task);
                            //await db.SaveChangesAsync();
                        }
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index", "Home");
                    }
                }                
            }
            return NotFound();
        }

    }
}
